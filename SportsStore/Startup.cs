﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SportsStore.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace SportsStore
{
    public class Startup
    {
        //,"ConnectionStringForWindows": "Server=(localdb)mssqlserver;Database=SportsStore;Trusted_Connection=TRUE;MultipleActiveResultSets=true;"
        public Startup(IConfiguration configuration) => Configuration = configuration;
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration["Data:SportStoreProducts:ConnectionString"]));
            //Register Repository
            services.AddTransient<IProductRepository, EFProductRepository>();
            //services.AddTransient<IProductRepository,FakeProductRepository>();
            services.AddScoped<Cart>(sp => SessionCart.GetCart(sp));
            services.AddSingleton<IHttpContextAccessor,HttpContextAccessor>();
            services.AddMvc();
            services.AddMemoryCache();
            services.AddSession();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseSession();
            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //});
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: null,
                template: "{category}/Page{productPage:int}",
                defaults: new { Controller = "Product", action = "List" });

               routes.MapRoute(
               name: null,
               template: "Page{productPage:int}",
               defaults: new { Controller = "Product", action = "List", productPage=1 });

               routes.MapRoute(
               name: null,
               template: "{category}",
               defaults: new { Controller = "Product", action = "List", productPage = 1 });

               routes.MapRoute(
               name: null,
               template: "",
               defaults: new { Controller = "Product", action = "List", productPage = 1 });

               routes.MapRoute(
                    name: null,
                    template: "{controller=Product}/{action=List}/{id?}");

                //routes.MapRoute(
                //  name: "pagination",
                //  template: "Product/Page{productPage}",
                //  defaults:new { Controller ="Product",action = "List" });

                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller=Product}/{action=List}/{id?}");
            });
            //SeedData.EnsurePopulated(app);
        }
    }
}
