﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SportsStore.Models;

namespace SportsStore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //var host = BuildWebHost(args).Migrate();

           // host.Run();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .Build();

        
    }

    public static class DBHelper {
        public static IWebHost Migrate(this IWebHost webhost)
        {
            using (var scope = webhost.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                using (var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>())
                {
                    dbContext.Database.Migrate();
                    if (!dbContext.Products.Any())
                    {
                        dbContext.Products.AddRange(new Product { Name = "Kayak", Description = "A boat for one person", Category = "Watersports", Price = 275 }
                                            , new Product { Name = "Lieftjacket", Description = "Protective and fasionable", Category = "Watersports", Price = 48.95m }
                                            , new Product { Name = "Soccer Ball", Description = "FIFA-approved size and weight", Category = "Soccer", Price = 19.50m }
                                            , new Product { Name = "Coner Flags", Description = "Give your playing field a professional touch", Category = "Soccer", Price = 34.95m }
                                            , new Product { Name = "Stadium", Description = "Flat-packed 35,000 seat stadium", Category = "Soccer", Price = 79500 }
                                            , new Product { Name = "Thinking Cap", Description = "Improve brain efficiency by 75%", Category = "Chess", Price = 16 }
                                            , new Product { Name = "Unsteady Chair", Description = "Secretly give your opponent a disadvantage", Category = "Chess", Price = 29.95m }
                                            , new Product { Name = "Human Chess Board", Description = "A fun game for a family", Category = "Chess", Price = 75 }
                                            , new Product { Name = "Bling-Bling King", Description = "Gold-plates, diamon-studded Kingy", Category = "Chess", Price = 1200 }
                                            );
                        dbContext.SaveChanges();
                    }
                }
            }
            return webhost;
        }
    }

    //public static void EnsurePopulated(IApplicationBuilder app)
    //{
    //    ApplicationDbContext ctx = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();
    //    ctx.Database.Migrate();
    //    //ctx.Database.EnsureCreated();
    //    if (!ctx.Products.Any())
    //    {
    //        ctx.Products.AddRange(new Product { Name = "Kayak", Description = "A boat for one person", Category = "Watersports", Price = 275 }
    //                            , new Product { Name = "Lieftjacket", Description = "Protective and fasionable", Category = "Watersports", Price = 48.95m }
    //                            , new Product { Name = "Soccer Ball", Description = "FIFA-approved size and weight", Category = "Soccer", Price = 19.50m }
    //                            , new Product { Name = "Coner Flags", Description = "Give your playing field a professional touch", Category = "Soccer", Price = 34.95m }
    //                            , new Product { Name = "Stadium", Description = "Flat-packed 35,000 seat stadium", Category = "Soccer", Price = 79500 }
    //                            , new Product { Name = "Thinking Cap", Description = "Improve brain efficiency by 75%", Category = "Chess", Price = 16 }
    //                            , new Product { Name = "Unsteady Chair", Description = "Secretly give your opponent a disadvantage", Category = "Chess", Price = 29.95m }
    //                            , new Product { Name = "Human Chess Board", Description = "A fun game for a family", Category = "Chess", Price = 75 }
    //                            , new Product { Name = "Bling-Bling King", Description = "Gold-plates, diamon-studded Kingy", Category = "Chess", Price = 1200 }
    //                            );
    //        ctx.SaveChanges();
    //    }
    //}
}
